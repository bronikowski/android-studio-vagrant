#!/bin/bash

vagrant status | grep "The VM is running" > /dev/null
if [ $? -eq 0 ]; then
	vagrant up
fi
vagrant ssh -c ./android-studio/bin/studio.sh
