# Android Studio in vagrant #

I hate installing work related software on my personal computer, this is just another instance of this behavior. A quick way to bring a disposable copy of Android Studio for times when I need to lookup something.

## Problems

1. It's slow. It's a full VM instance running Debian, running IDE written in Java, forwarded to VM's host
2. As far as I know Virtualbox does not carry over VT-x so your standard emulated images won't work
3. I've disabled USB because my UHCI is busted. I know, makes this whole thing pretty pointless from development PoV